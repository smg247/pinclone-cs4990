from django.conf.urls import patterns, url

from pinteresting import views

urlpatterns = patterns('',
    url(r'^$', views.ListPins.as_view(), name="index"), # Index page to show a list of latest entries
    url(r'^add/board/$', views.CreateBoard.as_view(), name='add_board'),
    url(r'^add/pin/$', views.CreatePin.as_view(), name='add_pin'),
    url(r'^repin/(?P<pk>\d+)/$', views.RePin.as_view(), name='repin'),
    url(r'^pin/update/(?P<pk>\d+)/$', views.EditPin.as_view(), name='update_pin'),
    url(r'^pin/delete/(?P<pk>\d+)/$', views.DeletePin.as_view(), name='delete_pin'),
    url(r'^board/(?P<pk>\d+)/$', views.BoardDetail.as_view(), name='board_detail'),
    url(r'^pin/(?P<pk>\d+)/$', views.PinDetail.as_view(), name='pin_detail'),
    url(r'^comment/(?P<pin_id>\d+)/$', views.CreateComment.as_view(), name='add_comment'),
    url(r'^comment/delete/(?P<pk>\d+)/$', views.DeleteComment.as_view(), name='delete_comment'),
    url(r'^comment/update/(?P<pk>\d+)/$', views.EditComment.as_view(), name='update_comment'),
)
