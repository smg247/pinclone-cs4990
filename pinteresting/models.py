from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


class Pin(models.Model):
    Colors = (
    ('Red', 'Red'),
    ('Blue', 'Blue'),
    ('Green', 'Green'),
    ('Grey', 'Grey'),
    ('Orange', 'Orange'),
    )


    title = models.CharField(max_length=30)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/', null=True)
    description = models.TextField()
    created_by = models.ForeignKey(User)
    pub_date = models.DateTimeField(auto_now_add=True)
    link = models.URLField(null=True, blank=True)
    is_public = models.BooleanField(default=True)
    board = models.ManyToManyField('Board')
    color = models.CharField(max_length=10, choices=Colors)

    @property
    def boards(self):
	return Board.objects.all().filter(pin=self)

    @property
    def num_comments(self):
	num = 0
	for comment in Comment.objects.all().filter(pin = self):
		num = num + 1
	return num

    def get_absolute_url(self):
	return reverse('pinteresting:index')

    def __unicode__(self):
        return self.title

class Comment(models.Model):
    user = models.ForeignKey(User)
    pin = models.ForeignKey(Pin)
    comment = models.TextField()

    def __unicode__(self):
        return self.comment

    def get_absolute_url(self):
	return reverse('pinteresting:pin_detail', args=(self.pin.id,))

class Board(models.Model):
    created_by = models.ForeignKey(User)
    name = models.CharField(max_length=100)


    @property
    def pins(self):
	return Pin.objects.all().filter(board=self)

    @property
    def public_pins(self):
	return Pin.objects.all().filter(board=self, is_public=True)

    def get_absolute_url(self):
	return reverse('pinteresting:add_pin')

    def __unicode__(self):
        return self.name

