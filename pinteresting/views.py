from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse, reverse_lazy
from registration.backends.simple.views import RegistrationView
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from endless_pagination.views import AjaxListView

from pinteresting.models import Comment, Board, Pin
from pinteresting.forms import AddBoard, AddPin, Repin, EditPin, CommentForm

class ListPins(AjaxListView):
	template_name = 'pinteresting/index.html'
	model = Pin
	context_object_name = 'pins'
	paginate_by = 5

	def get_queryset(self):
		return Pin.objects.filter(is_public=True).order_by('-pub_date')

	def get_context_data(self, **kwargs):
		user_id = self.request.user.id
		context = super(ListPins, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by__id = user_id)
		return context

class BoardDetail(DetailView):
	context_object_name = 'board'
	template_name = 'pinteresting/board_detail.html'

	def get_object(self):
		return get_object_or_404(Board, pk = self.kwargs['pk'])

	def get_context_data(self, **kwargs):
		user_id = self.request.user.id
		board = get_object_or_404(Board, pk = self.kwargs['pk'])
		cur_user = False
		if int(board.created_by.id) == user_id:#make sure this still works when logged in
			cur_user = True
 
		context = super(BoardDetail, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by__id = user_id)
		context['cur_user'] = cur_user
		return context

class PinDetail(DetailView):
	context_object_name = 'pin'
	template_name = 'pinteresting/pin_detail.html'

	def get_object(self):
		return get_object_or_404(Pin, pk = self.kwargs['pk'])

	def get_context_data(self, **kwargs):
		user_id = self.request.user.id
		pin = get_object_or_404(Pin, pk = self.kwargs['pk'])
		cur_user = False
		if int(pin.created_by.id) == user_id:
			cur_user = True

		context = super(PinDetail, self).get_context_data(**kwargs)

		if user_id is not None:
			user = self.request.user
			context['user'] = user

		context['user_boards'] = Board.objects.filter(created_by__id = user_id)
		context['cur_user'] = cur_user
		context['comments'] = Comment.objects.filter(pin = pin)
		
		return context


class CreateBoard(CreateView):
	model = Board
	template_name = 'pinteresting/board_create.html'
	form_class = AddBoard

	def get_form(self, form_class):
		user = self.request.user
		form = super(CreateBoard, self).get_form(form_class)
		form.instance.created_by = user
		return form

	def get_context_data(self, **kwargs):
		user = self.request.user
		context = super(CreateBoard, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		return context

	def form_valid(self, form):
		messages.success(self.request, 'Board successfully created!')
		return super(CreateBoard, self).form_valid(form)
	
	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(CreateBoard, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(CreateBoard, self).dispatch(*args, **kwargs)

class CreatePin(CreateView):
	model = Pin
	template_name = 'pinteresting/pin_create.html'
	form_class = AddPin

	def get_form(self, form_class):
		user = self.request.user
		form = super(CreatePin, self).get_form(form_class)
		form.instance.created_by = user
		form.fields['board'].queryset = Board.objects.filter(created_by = user)
		return form

	def get_context_data(self, **kwargs):
		user = self.request.user
		boards_avail = False
		for board in Board.objects.all():#checking all boards to see if current user has any
			if int(board.created_by.id) == int(user.id):
				boards_avail = True
				break

		context = super(CreatePin, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		context['boards_avail'] = boards_avail
		return context

	def form_valid(self, form):
		messages.success(self.request, 'Pin successfully created!')
		return super(CreatePin, self).form_valid(form)
	
	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(CreatePin, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(CreatePin, self).dispatch(*args, **kwargs)


class EditPin(UpdateView):
	model = Pin
	template_name = 'pinteresting/pin_update.html'
	form_class = EditPin

	def get_form(self, form_class):
		user = self.request.user
		form = super(EditPin, self).get_form(form_class)
		form.instance.created_by = user
		form.fields['board'].queryset = Board.objects.filter(created_by = user)
		return form

	def get_context_data(self, **kwargs):
		user = self.request.user
		boards_avail = False
		for board in Board.objects.all():#checking all boards to see if current user has any
			if int(board.created_by.id) == int(user.id):
				boards_avail = True
				break
		pin = get_object_or_404(Pin, pk = self.kwargs['pk'])
		cur_user = False
		if int(pin.created_by.id) == int(user.id):
			cur_user = True
		context = super(EditPin, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		context['boards_avail'] = boards_avail
		context['cur_user'] = cur_user
		return context

	def form_valid(self, form):
		messages.success(self.request, 'Pin successfully updated!')
		return super(EditPin, self).form_valid(form)
	
	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(EditPin, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(EditPin, self).dispatch(*args, **kwargs)


class DeletePin(DeleteView):
	model = Pin
	success_url = reverse_lazy('pinteresting:index')

	def get_context_data(self, **kwargs):
		user = self.request.user
		pin = get_object_or_404(Pin, pk = self.kwargs['pk'])
		cur_user = False
		if int(pin.created_by.id) == int(user.id):
			cur_user = True
		context = super(DeletePin, self).get_context_data(**kwargs)
		context['cur_user'] = cur_user
		return context

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(DeletePin, self).dispatch(*args, **kwargs)


class RePin(CreateView):#mixin
	model = Pin
	template_name = 'pinteresting/repin.html'
	form_class = Repin

	def get_form(self, form_class):
		user = self.request.user
		pin = get_object_or_404(Pin, pk = self.kwargs['pk'])
		form = super(RePin, self).get_form(form_class)
		form.fields['board'].queryset = Board.objects.filter(created_by = user)
		form.instance.created_by = user #copying all the details over
		form.instance.title = pin.title
		form.instance.image = pin.image
		form.instance.description = "Repinned from " + str(pin.created_by) + ": " + str(pin.description)
		form.instance.link = pin.link
		form.instance.color = pin.color
		return form

	def get_context_data(self, **kwargs):
		user = self.request.user
		boards_avail = False
		for board in Board.objects.all():#checking all boards to see if current user has any
			if int(board.created_by.id) == int(user.id):
				boards_avail = True
				break

		context = super(RePin, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		context['boards_avail'] = boards_avail
		return context

	def form_valid(self, form):
		self.object = form.save(commit=False)
		messages.success(self.request, 'Pin successfully repinned!')
		return super(RePin, self).form_valid(form)

	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(RePin, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(RePin, self).dispatch(*args, **kwargs)

class CreateComment(CreateView):
	model = Comment
	template_name = 'pinteresting/comment_create.html'
	form_class = CommentForm

	def get_form(self, form_class):
		user = self.request.user
		pin = get_object_or_404(Pin, pk = self.kwargs['pin_id'])
		form = super(CreateComment, self).get_form(form_class)
		form.instance.user = user
		form.instance.pin = pin
		return form

	def get_context_data(self, **kwargs):
		user = self.request.user
		pin = get_object_or_404(Pin, pk = self.kwargs['pin_id'])
		context = super(CreateComment, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		context['pin'] = pin
		return context

	def form_valid(self, form):
		messages.success(self.request, 'Comment successfully added!')
		return super(CreateComment, self).form_valid(form)
	
	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(CreateComment, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(CreateComment, self).dispatch(*args, **kwargs)


class DeleteComment(DeleteView):
	model = Comment
	success_url = reverse_lazy('pinteresting:index')

	def get_context_data(self, **kwargs):
		user = self.request.user
		comment = get_object_or_404(Comment, pk = self.kwargs['pk'])
		cur_user = False
		if int(comment.user.id) == int(user.id):
			cur_user = True
		context = super(DeleteComment, self).get_context_data(**kwargs)
		context['cur_user'] = cur_user
		return context
	
	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(DeleteComment, self).dispatch(*args, **kwargs)

class EditComment(UpdateView):
	model = Comment
	template_name = 'pinteresting/comment_update.html'
	form_class = CommentForm

	def get_context_data(self, **kwargs):
		user = self.request.user
		context = super(EditComment, self).get_context_data(**kwargs)
		context['user_boards'] = Board.objects.filter(created_by = user)
		return context

	def form_valid(self, form):
		messages.success(self.request, 'Comment successfully updated!')
		return super(EditComment, self).form_valid(form)
	
	def form_invalid(self, form):
		messages.error(self.request, 'Something went wrong please try again!')
		return super(EditComment, self).form_invalid(form)

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(EditComment, self).dispatch(*args, **kwargs)

class MyRegistrationBackend(RegistrationView):
    def get_success_url(self, request, user):
        return reverse('pinteresting:index')

