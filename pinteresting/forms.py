from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from pinteresting.models import Pin, Board, Comment

class AddBoard(ModelForm):
	class Meta:
		model = Board
		fields = ['name']

class AddPin(ModelForm):
	class Meta:
		model = Pin
		exclude = ('pub_date','created_by',)

class Repin(ModelForm):
	class Meta:
		model = Pin
		fields = ['board']

class EditPin(ModelForm):
	class Meta:
		model = Pin
		exclude = ('pub_date','created_by',)

class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ['comment']
